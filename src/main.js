import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.jsx';

let rootNode = document.getElementById('main');

ReactDOM.render(
    <App />, rootNode
);