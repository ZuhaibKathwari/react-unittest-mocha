import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.jsx';

describe('Main component', () => {
    it('rendered without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<App />, div);
    });
});