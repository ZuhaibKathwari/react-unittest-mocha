class AppSessionStore {
    constructor(){
      this.isSessionStorageSupported = this.isSessionStorageSupported.bind(this);
    }
  
    isSessionStorageSupported () {
          try {
              const storage = window.sessionStorage;
              storage.setItem("testProp", "__storage_test__");
              storage.removeItem("testProp");
  
              return {
                  getItem: (key) => {
                      const value = window.sessionStorage.getItem(key);
                      return value !== null ? JSON.parse(value) : null;
                  },
                  setItem: (key, value) => {
                      try {
                          window.sessionStorage.setItem(key, JSON.stringify(value));
                          return true;
                      } catch (e) {
                          console.log(e);
                          return false;
                      }
                  }
              };
  
          } catch (e) {
            throw "SessionStorage not enabled";
            return false;
          }
      }
  }
  
  export default AppSessionStore.prototype.isSessionStorageSupported();