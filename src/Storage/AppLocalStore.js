class LocalStore {
  constructor(){
    this.isLocalStorageSupported = this.isLocalStorageSupported.bind(this);
  }

  isLocalStorageSupported () {
        try {
            const storage = window.localStorage;
            storage.setItem("testProp", "__storage_test__");
            storage.removeItem("testProp");

            return {
                getItem: (key) => {
                    const value = window.localStorage.getItem(key);
                    return value !== null ? JSON.parse(value) : null;
                },
                setItem: (key, value) => {
                    try {
                        window.localStorage.setItem(key, JSON.stringify(value));
                        return true;
                    } catch (e) {
                        console.log(e);
                        return false;
                    }
                }
            };

        } catch (e) {
          throw "LocalStorage not enabled";
          return false;
        }
    }
}

export default LocalStore.prototype.isLocalStorageSupported();