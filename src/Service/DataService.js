import AppSessionStore from './../Storage/AppSessionStore.js';

class DataService {
    constructor() {
        this.getPreviousMeterRead = this.getPreviousMeterRead.bind(this);
    }

    ParseJSON (response) {
        return response.text().then((text) => {
            return text ? JSON.parse(text) : {}
        })
    }

    getLastMeterRead () {
        let requestObj = {
            URL:  "https://test.connect.boomi.com",
            URI: "/ws/rest/internal/aem/UK/installations/2100119/meters/lastreads"
        };

        if (AppSessionStore.getItem("LastMeterRead") === null)
        {
            fetch(`${requestObj.URL}${requestObj.URI}` + ";boomi_auth=c2FsZXNmb3JjZUBlb25idXNpbmVzc3NlcnZpY2VzLVlHRUhZOS5SMkNZNzg6ZmM5ZmU1NDItYWJlNi00OTg0LTlhM2QtNzhiNjAxODlhZGUy", {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                },
                credentials: 'include',
                mode: 'cors'
            })
                .then(response => this.ParseJSON(response))
                .then(json => {
                    console.log('parsed json', json);
                    //LocalStore.setItem("LastMeterRead", JSON.stringify(json));
                    AppSessionStore.setItem("LastMeterRead", JSON.stringify(json));

                }).catch((e) => {
                throw e;
            })
        }
        else
        {
            console.log('Key is already set');
        }

    }
}

export default DataService.prototype;
