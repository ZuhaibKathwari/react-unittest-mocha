import React, {Component} from 'react';

class ShowComments extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.state = {
            Comments: this.props.Comments
        };
    }

    render () {
        let commentItems = this.state.Comments.map((item, key) => {
            return (<p key={key}>{item}</p>);
        });
        return (
            <div className="comment-display" >
                <h3>Your Comment 's:</h3>
                {commentItems}
            </div>
        );
    }

}

export default ShowComments;
