import React, {Component} from 'react';
import CommentArea from './CommentArea.jsx';
import DataService from './../Service/DataService.js';

class Commentrator extends Component {

    constructor(props) {
        super(props);
        this.show = false;
        this.dataService = DataService;
        this.state = {
            commentArea: this.show
        };

        this.showCommentArea = this.showCommentArea.bind(this);
        this.testAPI = this.testAPI.bind(this);
    }

    showCommentArea () {
        this.show = !this.show;
        this.setState({ commentArea: this.show });
    };

    testAPI () {
        this.dataService.getLastMeterRead();
    };

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <h1>Commenterator!</h1>
                </header>
                <div>
                    <div className="comment-button">
                        <input type="button" value="Enter your comment's" onClick={this.showCommentArea}/>
                        <input type="button" value="Check Request Sire!" onClick={this.testAPI}/>
                    </div>
                    {this.state.commentArea ? <CommentArea /> : null}
                </div>
            </div>
        );
    }
}

export default Commentrator;