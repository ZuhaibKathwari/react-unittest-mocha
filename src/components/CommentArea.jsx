import React, {Component} from 'react';
import ShowComments from './ShowComments.jsx';

class CommentArea extends Component {
    constructor(props) {
        super(props);
        this.newArr = [];
        this.state = {
            textValue: "",
            Comments: [],
            showComments: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.Clear = this.Clear.bind(this);
    }

    handleChange (event) {
        this.setState({
            textValue: event.target.value
        });
        event.preventDefault();
    };

    handleSubmit () {
        this.state.textValue !== '' ? this.newArr.push(this.state.textValue) : null;
        this.setState({
            Comments: this.newArr,
            showComments: true,
            textValue: ''
        });
    };

    Clear () {
        this.setState({
            Comments: [],
            showComments: false
        });
        this.newArr = [];
    }

    render() {
        return (
            <div className="comment-area">
                <form name="commentForm">
                    <div>
                        <textarea value={this.state.textValue} onChange={this.handleChange}></textarea>
                    </div>
                    <div>
                        <input type="button" className="submit-button" value="Save" onClick={this.handleSubmit}/>
                        <input type="button" className="cancel-button" onClick={this.Clear} value="Clear" />
                    </div>
                </form>
                {this.state.showComments ? <ShowComments Comments={this.state.Comments}/> : null}
            </div>
        );
    }
}

export default CommentArea;