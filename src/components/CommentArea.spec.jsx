import React, {Component} from 'react';
import CommentArea from './CommentArea';
import {expect} from 'chai';
import ShowComments from './ShowComments';
import { shallow } from 'enzyme';

describe('CommentArea component', () => {
    it ('should shallow render ShowComments component', () => {
        shallow(<ShowComments Comments={[1,2,3,4,5]}/>);
    });

    it ('should call handleChange subroutine', () => {
        const commentarea = shallow(<CommentArea />);
        let event = {target: {}, preventDefault: () => {}}
        commentarea.instance().handleChange(event);
    });

    it ('should call handleSubmit subroutine', () => {
        const commentarea = shallow(<CommentArea />);
        // let event = {target: {}, preventDefault: () => {}}
        commentarea.instance().handleSubmit();
    });
});