import CommentArea from './CommentArea';
import React, {Component} from 'react';
import Commentrator from './Commentrator';
import {expect} from 'chai';
import { shallow } from 'enzyme';

describe('commentrator component', () => {
        it ('should shallow render CommentArea component', () => {
            const commentarea = shallow(<CommentArea />);
            expect(commentarea.contains(<li/>)).to.equal(false);
        });

    it ('should call showCommentArea subroutine', () => {
        const wrapper = shallow(<Commentrator />);
        wrapper.instance().showCommentArea();
    });

});

