import React, {Component} from 'react';
import Commentrator from './Commentrator.jsx';

class App extends Component {
    constructor() {
        super();
    }

    render () {
        return (
            <Commentrator />
        );
    }

}

export default App;