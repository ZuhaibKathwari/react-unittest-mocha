import gulp from 'gulp';
import browserify from 'browserify';
import source from "vinyl-source-stream";
import babelify from 'babelify';
import connect from 'gulp-connect';

gulp.task('browserify', function() {
    let b = browserify({
        entries: ['src/main.js'],
        debug: true
    });
    b.transform(babelify, {presets: ['es2015', 'react']});
    return b.bundle()
        .pipe(source('main.js'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('connect', function() {
    connect.server(
        {
            root: '.',
            port: 3000,
            livereload: true
        }
    );
});

gulp.task('files', function () {
    gulp.src('src/**/*.jsx')
        .pipe(connect.reload());
});

gulp.task('watch', function() {
    gulp.watch('src/*.js', ['browserify']);
    gulp.watch('src/**/*.jsx', ['browserify']);
    gulp.watch('files');
});

gulp.task('default', ['connect', 'watch']);